if("serviceWorker" in navigator){
    window.addEventListener("load", () => {
      navigator.serviceWorker
        .register("/service-worker.js")
        .then(() => {
          console.log("pendaftaran SeviceWorker berhasil")
        })
        .catch(() => {
          console.log('Pendaftaran ServiceWorker gagal')
        });
    });
} else {
    console.log('ServiceWorker belum didukung browser ini')
}