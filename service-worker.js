const CACHE_NAME = "kembangku-serviceV1";
var urlsToCache = [
  "/",
  "/manifest.json",
  "/nav.html",
  "/index.html",
  "/page/about.html",
  "/page/contact.html",
  "/page/home.html",
  "/page/gallery.html",
  "/css/custom.css",
  "/css/materialize.min.css",
  "/js/materialize.min.js",
  "/js/nav.js",
  "/js/config.js",
  "/images/agrii.png",
  "/images/icon-192x192.png",
  "/images/icon-512x512.png",
  "/images/plant.png",
  "/images/plants3.jpg",
  "/images/plants4.jpg",
  "/images/plants7.jpg",
  "/images/tree.png",
  "/images/worldwide.png",
  "/images/maskable_icon-192x192.png",
  "https://fonts.googleapis.com/icon?family=Material+Icons",
  "https://fonts.gstatic.com/s/materialicons/v55/flUhRq6tzZclQEJ-Vdg-IuiaDsNc.woff2",
];

self.addEventListener("install", function (event) {
  event.waitUntil(
    caches.open(CACHE_NAME).then(function (cache) {
      return cache.addAll(urlsToCache);
    })
  );
});
self.addEventListener("fetch", function (event) {
  event.respondWith(
    caches
      .match(event.request, { cacheName: CACHE_NAME })
      .then(function (response) {
        if (response) {
          console.log("ServiceWorker: Gunakan aset dari cache: ", response.url);
          return response;
        }

        console.log(
          "ServiceWorker: memuat aset dari server",
          event.request.url
        );

        return fetch(event.request);
      })
  );
});
self.addEventListener("activate", function (event) {
  event.waitUntil(
    caches.keys().then(function (cahceNames) {
      return Promise.all(
        cahceNames.map(function (cacheName) {
          if (cacheName != CACHE_NAME) {
            console.log("ServiceWorker: cache" + cacheName + "dihapus");
            return caches.delete(cacheName);
          }
        })
      );
    })
  );
});
